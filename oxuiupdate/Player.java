package oxuiupdate;


public class Player {
	//this file update 13/8/2562.
	private char name;
	int win , lose, draw;

	public Player(char name) {
     this.name = name;
     win = 0 ;
     lose= 0 ;
     draw = 0;
     
	}

	public char get_Name() {
		return name;
	}

	public int get_Win() {
		return win;
	}

	public int get_lose() {
		return lose;
	}

	public int get_draw() {
		return draw;
	}
	public void Win() {
		win++;
	}
	public void lose() {
		lose++;
	}
	public void draw() {
		draw++;
	}
	
	
}
